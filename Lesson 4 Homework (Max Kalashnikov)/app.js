// Product
/*
name
price
image
discount
 */
(function($) {
    const $products = $('#main li.product');
    const products = [];
    const currencySymbol = $('.woocommerce-Price-currencySymbol').first().text();
    $products.each((i, p) => {
        const product = {
            name: $(p).find('.woocommerce-loop-product__title').text(),
            price: parseFloat($(p).find('.price .amount').first().text().replace(currencySymbol, '')),
            image: $(p).find('img').attr('src'),
            discount: '',
        };

        if ($(p).find('.onsale').length) {
            let priceRegular = product.price;
            let priceSale = parseFloat($(p).find('.price .amount').last().text().replace(currencySymbol, ''));
            product.discount = (priceRegular - priceSale) / priceRegular * 100;
        }

        products.push(product);
    });
    const productsJSON = JSON.stringify(products);
})(jQuery);