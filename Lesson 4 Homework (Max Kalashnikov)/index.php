<?php

/*
    Убрал Math.round для просчета процента скидки, чтобы соблюсти цифры из источника данных. Теперь процент скидки, где он есть, идет Float значением
 */
$productsJSON = '[{"name":"2 Pack Toothbrushes","price":3.7,"image":"//themes.woocommerce.com/pharmacy/wp-content/uploads/sites/89/2015/11/Fotolia_69220007_Subscription_Monthly_M_blue.jpg?w=600&h=600&crop=1","discount":""},{"name":"Advanced Moisturiser","price":27,"image":"//themes.woocommerce.com/pharmacy/wp-content/uploads/sites/89/2013/06/Fotolia_49730269_Subscription_Monthly_M.jpg?w=600&h=600&crop=1","discount":""},{"name":"Allergy & Hayfever Tablets","price":5.19,"image":"//themes.woocommerce.com/pharmacy/wp-content/uploads/sites/89/2013/06/Fotolia_82577916_Subscription_Monthly_M.jpg?w=600&h=600&crop=1","discount":""},{"name":"Anti-Ageing Face Cream","price":34.99,"image":"//themes.woocommerce.com/pharmacy/wp-content/uploads/sites/89/2013/06/Fotolia_80657285_Subscription_Monthly_M.jpg?w=600&h=600&crop=1","discount":11.44},{"name":"Antiseptic Healing Cream","price":2.6,"image":"//themes.woocommerce.com/pharmacy/wp-content/uploads/sites/89/2013/06/Fotolia_94095546_Subscription_Monthly_M.jpg?w=600&h=600&crop=1","discount":""},{"name":"Aviator Sunglasses","price":99,"image":"//themes.woocommerce.com/pharmacy/wp-content/uploads/sites/89/2015/11/Fotolia_67023246_Subscription_Monthly_M.jpg?w=600&h=600&crop=1","discount":""},{"name":"Back Pain Relief Tablets","price":22,"image":"//themes.woocommerce.com/pharmacy/wp-content/uploads/sites/89/2013/06/Fotolia_82577911_Subscription_Monthly_M.jpg?w=600&h=600&crop=1","discount":""},{"name":"Basic Black & White Glasses","price":79,"image":"//themes.woocommerce.com/pharmacy/wp-content/uploads/sites/89/2015/11/Fotolia_72667708_Subscription_Monthly_M.jpg?w=600&h=600&crop=1","discount":""},{"name":"Chesty Cough Syrup","price":12.99,"image":"//themes.woocommerce.com/pharmacy/wp-content/uploads/sites/89/2013/06/Fotolia_93010142_Subscription_Monthly_M.jpg?w=600&h=600&crop=1","discount":23.1},{"name":"Conditioner Shiny Hair","price":15,"image":"//themes.woocommerce.com/pharmacy/wp-content/uploads/sites/89/2013/06/Fotolia_54785159_Subscription_Monthly_M.jpg?w=600&h=600&crop=1","discount":6.74}]';

$products  = json_decode($productsJSON);
$taxPerc   = 9.3;
$subtotal  = 0;
$taxAmount = 0;

foreach ($products as $key => $product) :        
    
    $product_price    = (!empty($product->discount)) ? $product->price - ($product->price * ($product->discount / 100)) : $product->price;
    $background_color = ($key % 2) ? '#6d6d6d' : '#dedede';

    //Закомментировал вывод фотографии для простоты дебага
    $tableBody .= "
        <tr class='item-row' style='background-color:".$background_color."'>
            <td>
                <input type='number' min='1' value='1' data-single-item-price='".number_format($product->price, 2, '.', '')."' class='product-quantity' style='max-width: 40px;' />
            </td>
            <td>
                <!-- <img style=width: 100%; height: auto; max-width: 150px; src=".$product->image." alt='".$product->name."'> -->
                <span style=display: block;>".$product->name."</span>
            </td>
            <td class=single-product-subtotal>".number_format($product_price, 2, '.', '')."</td>
        </tr>";

    $subtotal += $product_price;

endforeach;

$taxAmount = number_format( ($subtotal * ($taxPerc / 100)), 2, '.', '' );
$total     = number_format($subtotal + $taxAmount, 2, '.', '');

$html = <<<HTML

<table class="table table-striped">
    <thead>
        <tr>
            <th>Qty</th>
            <th>Product</th>
            <th>Subtotal</th>
        </tr>
    </thead>
    <tbody>
        $tableBody
    </tbody>
</table>
<table class="total-table table">
    <tbody>
        <tr>
            <th class="tax-rate" data-tax-rate="$taxPerc">
                Tax ($taxPerc%)
            </th>
            <td>$<span class="tax-amount">$taxAmount</span></td>
        </tr>
        <tr>
            <th>Total:</th>
            <td>$<span class="total-price">$total</span></td>
        </tr>
    </tbody>
</table>
HTML;

echo $html;

/*  
    Проверочные рассчеты

    Дефолтные значения
    3,7+27+5,19+30,99+2,6+99+22+79+9,99+13,99 = 293,46
    9,3% = 27,29178 (27,29)

    293,46+27,29178 = 320,75178 (320,75)
    ===================================================================

    Добавил единицу к первой строке
    7,4+27+5,19+30,99+2,6+99+22+79+9,99+13,99 = 297,16
    9,3% = 27,63588 (27,64)

    297,16+27,63588 = 324,79588 (324.80)
    ===================================================================

    Добавил единицу к 6 строке
    7,4+27+5,19+30,99+2,6+198+22+79+9,99+13,99 = 396,16
    9,3% = 36,84288 (36,84)

    396,16+36,84288 = 433,00288 (433,00)
    ===================================================================
    
    Добавил 11 к 5 строке
    7,4+27+5,19+30,99+31,2+198+22+79+9,99+13,99 = 424,76

    9,3% = 39,50268 (39,50)

    424,76+39,5 = 464,26268 (464,26)
 */
?>


<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.6.0/jquery.min.js" integrity="sha512-894YE6QWD5I59HgZOGReFYm4dnWc1Qt5NtvYSaNcOP+u1T9qYdvdihz0PPSiiqn/+/3e7Jo4EaG7TubfWGUrMQ==" crossorigin="anonymous"></script>

<!--place for jQuery -->
<script type="application/javascript">
    //jQuery + функции
    // (function($) {
    //     function calculateItemTotal(firedInput, itemQuantity, singleItemPrice) {
    //         let itemCurrentSubtotal = firedInput.parents('tr').find('.single-product-subtotal');
    //         let itemCalculatedSubtotal = (itemQuantity * singleItemPrice);

    //         itemCurrentSubtotal.text(itemCalculatedSubtotal.toFixed(2));
    //     }

    //     function calculateTaxAmount(taxRate, subtotal) {
    //         let taxAmount = subtotal*(taxRate/100);
    //         $('.tax-amount').text(taxAmount.toFixed(2));

    //         return taxAmount;
    //     }

    //     function calculateAbsoluteTotal(taxRate){
    //         let subtotal = 0;
    //         $('.single-product-subtotal').each((i, singleProductSubtotal) => {
    //             subtotal += parseFloat($(singleProductSubtotal).text());
    //         })

    //         let taxAmount = calculateTaxAmount(taxRate, subtotal);

    //         let totalPrice = subtotal+taxAmount;
    //         $('.total-price').text(totalPrice.toFixed(2));
    //     }

    //     $(document).ready(function(){
    //         $('.product-quantity').on('change', function() {

    //             let firedInput = $(this);

    //             if(firedInput.val() < 1){
    //                 firedInput.val(1);
    //                 return;
    //             }

    //             let itemQuantity = parseInt(firedInput.val());
    //             let singleItemPrice = parseFloat(firedInput.data('single-item-price'));

    //             calculateItemTotal(firedInput, itemQuantity, singleItemPrice);

    //             let taxRate = $('.tax-rate').data('tax-rate');
    //             calculateAbsoluteTotal(taxRate);
    //         });
    //     });
    // })(jQuery);

    //Потому что need mooooooooooooore objects
    (function($) {
        $(document).ready(function(){
            $('.product-quantity').on('change', function() {

                let firedInput = $(this);

                if(firedInput.val() < 1){
                    firedInput.val(1);
                    return;
                }

                function Calculator(firedInput, itemQuantity, singleItemPrice, taxRate) {
                    this.firedInput = firedInput;
                    this.itemQuantity = itemQuantity;
                    this.singleItemPrice = singleItemPrice;
                    this.taxRate = taxRate;

                    this.calculateItemTotal = () => {
                        let itemCurrentSubtotal = this.firedInput.parents('tr').find('.single-product-subtotal');
                        let itemCalculatedSubtotal = (this.itemQuantity * this.singleItemPrice);

                        itemCurrentSubtotal.text(itemCalculatedSubtotal.toFixed(2));
                    }

                    this.calculateTaxAmount = (taxRate, subtotal) => { 
                        let taxAmount = subtotal*(this.taxRate/100);
                        $('.tax-amount').text(taxAmount.toFixed(2));

                        return taxAmount;
                    }

                    this.calculateAbsoluteTotal = () => {
                        let subtotal = 0;

                        $('.single-product-subtotal').each((i, singleProductSubtotal) => {
                            subtotal += parseFloat($(singleProductSubtotal).text());
                        })

                        let taxAmount = this.calculateTaxAmount(taxRate, subtotal);

                        let totalPrice = subtotal+taxAmount;
                        $('.total-price').text(totalPrice.toFixed(2));
                    }
                }
                
                let totalCalc = new Calculator(firedInput, parseInt(firedInput.val()), parseFloat(firedInput.data('single-item-price')), $('.tax-rate').data('tax-rate'));

                totalCalc.calculateItemTotal();

                totalCalc.calculateAbsoluteTotal();
            });
        });
    })(jQuery);
</script>
<?php ?>